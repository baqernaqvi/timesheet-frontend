# README
## NAME
TimeSheet
## DESCRIPTION
A simple **TimeSheet** application used to record the time logs of employees. Its a frontend of **TimeSheet** app developed in **Angular5** with **Bootstrap**

## INSTALLATION
### USING [NPM](https://www.npmjs.com/get-npm)

#### Cloning repository
Clone this repository by running this command in the terminal
```bash
git clone https://baqernaqvi@bitbucket.org/baqernaqvi/timesheet-frontend.git
```

#### Move to the Front-End Directory
```bash
cd timesheet-frontend
```
#### Install Dependencies
```bash
npm install
```
#### Development server
Run ``` npm run start ``` for a dev server. Navigate to <http://localhost:4200>. The app will automatically reload if you change any of the source files.

## USAGE
Can log to keep a track of what an employee did in the week. An employee can add logs for a week only. For Example: if it is Sunday today so employee can log only from past Saturday to current day Sunday