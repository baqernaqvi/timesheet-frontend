import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import { environment } from '../environments/environment';

@Injectable()
export class ServicesProvider {
  apiUrl = environment.baseUrl;
  constructor(public http: HttpClient) { }

  getEmployees() {
    return this.http.get<Array<any>>(this.apiUrl + '/employee/getall');
  }
  getTimeSheetData(empId, week){
    return this.http.get<Array<any>>(this.apiUrl + `/timelog/get?employeeId=${empId}&&week=${week}`);
  }
  getTaskData(){
    return this.http.get<Array<any>>(this.apiUrl + '/task/getall');
  }
  addTimeLog(logData){
    return this.http.post(this.apiUrl + "/timelog/add", logData); 
  }
} 
