import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesProvider } from '../../../providers/services'
@Component({
  templateUrl: './time-log.component.html',
  styleUrls: [
    './time-log.component.scss',
    '../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class TimeLogDataComponent implements OnInit {
  users : any;
  employee: Object = {};
  tasks: Array<Object> = [];
  timeLog = {
    TaskId: 1,
    EmployeeId: null,
    Day: '',
    Hours: null
  }
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(public service: ServicesProvider, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.getEmployeesData();
    this.getTasks();
  }
  getTasks(){
    this.service.getTaskData().subscribe(data => {
      this.tasks = data;
    })
  }
  setEmployeeData(data){
    this.route.params.subscribe(params => {
      this.timeLog.EmployeeId = +params['id']; // (+) converts string 'id' to a number
      data.forEach(element => {
        if(element.Id == +params['id'])
          this.employee = element
      });
   })
  }
  getEmployeesData(){
    this.service.getEmployees().subscribe(data => {
      this.users = data;
      this.setEmployeeData(data);
    })
  }
  addTimeLog(){
    console.log(this.timeLog)
    this.service.addTimeLog(this.timeLog).subscribe(data => {
     this.navigateToTimesheet();
    })
  }
  navigateToTimesheet(){
    this.router.navigate(['/timeSheet', this.timeLog.EmployeeId]);
  }
}
