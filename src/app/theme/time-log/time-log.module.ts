import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeLogDataComponent } from './time-log.component';
import {TimeLogDataRoutingModule} from './time-log-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TimeLogDataRoutingModule,
    SharedModule,
    NgxDatatableModule,
    FormsModule
  ],
  declarations: [TimeLogDataComponent]
})
export class TimeLogModule { }
