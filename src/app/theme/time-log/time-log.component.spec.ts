import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimeLogDataComponent } from './time-log.component';

describe('TimeLogDataComponent', () => {
  let component: TimeLogDataComponent;
  let fixture: ComponentFixture<TimeLogDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimeLogDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimeLogDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
