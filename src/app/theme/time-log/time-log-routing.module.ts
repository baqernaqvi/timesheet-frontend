import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TimeLogDataComponent} from './time-log.component';

const routes: Routes = [
  {
    path: '',
    component: TimeLogDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeLogDataRoutingModule { }
