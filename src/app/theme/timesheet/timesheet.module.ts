import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimeSheetDataComponent } from './timesheet.component';
import {TimeSheetDataRoutingModule} from './timesheet-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    TimeSheetDataRoutingModule,
    SharedModule,
    NgxDatatableModule,
    FormsModule
  ],
  declarations: [TimeSheetDataComponent]
})
export class TimeSheetModule { }
