import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute } from '@angular/router';
import { ServicesProvider } from '../../../providers/services';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: [
    './timesheet.component.scss',
    '../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class TimeSheetDataComponent implements OnInit {
  rowsBasic = [];
  loadingIndicator = true;
  reorderable = true;
  employeeId: number;
  columns = [
    { prop: 'TaskName' },
    { prop: 'Day' },
    { prop: 'Hours' },
  ];

  expanded = {};

  users : any;
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(public service: ServicesProvider, private route: ActivatedRoute, private router: Router) {  }

  ngOnInit() {
    this.getEmployeesData();
    this.getTimeLogData();
  }
  setEmployeeId(){
    this.route.params.subscribe(params => {
      this.employeeId = +params['id']; // (+) converts string 'id' to a number
   });
  }
  updateRouteParams(){
    this.router.navigate(['/timeSheet/' + this.employeeId])
    //this.setEmployeeId();
    this.getTimeLogData();
  }
  getEmployeesData(){
    this.setEmployeeId();
    this.service.getEmployees().subscribe(data => {
      this.users = data
    })
  }
  getTimeLogData(){
    this.service.getTimeSheetData(this.employeeId, 1).subscribe(data => {
      this.rowsBasic = data
    })
  }
  addTimeLog(){
    this.router.navigate(['/addTimeLog', this.employeeId]);
  }
}
