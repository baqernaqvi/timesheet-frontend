import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TimeSheetDataComponent} from './timesheet.component';

const routes: Routes = [
  {
    path: '',
    component: TimeSheetDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimeSheetDataRoutingModule { }
