import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EmployeesDataComponent} from './employees.component';

const routes: Routes = [
  {
    path: '',
    component: EmployeesDataComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesDataRoutingModule { }
