import { Component, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { Router, CanActivate } from '@angular/router';
import { ServicesProvider } from '../../../providers/services'
@Component({
  templateUrl: './employees.component.html',
  styleUrls: [
    './employees.component.scss',
    '../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class EmployeesDataComponent implements OnInit {
  rowsBasic: any[] = [];
  loadingIndicator = true;
  reorderable = true;

  columns = [
    { prop: 'Id' },
    { prop: 'Code' },
    { prop: 'Name' }
  ];
  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(public service: ServicesProvider, private router: Router) { }

  ngOnInit() {
    this.getEmployeesData();
  }
  getEmployeesData() {
    this.service.getEmployees().subscribe(data => {
      this.rowsBasic = data
      console.log(this.rowsBasic);
    })
  }
  onActivate(event: any) {
    if (event.type == 'click') {
      this.router.navigate(['/timeSheet', event.row.Id]);
    }
  }
}
