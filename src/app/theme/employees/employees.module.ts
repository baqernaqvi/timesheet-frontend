import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmployeesDataComponent } from './employees.component';
import {EmployeesDataRoutingModule} from './employees-routing.module';
import {SharedModule} from '../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    EmployeesDataRoutingModule,
    SharedModule,
    NgxDatatableModule
  ],
  declarations: [EmployeesDataComponent]
})
export class EmployeesModule { }
