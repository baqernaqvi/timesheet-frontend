import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AdminComponent} from './layout/admin/admin.component';

const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'timeSheet/:id',
        loadChildren: './theme/timesheet/timesheet.module#TimeSheetModule'
      },
      {
        path: 'employees',
        loadChildren: './theme/employees/employees.module#EmployeesModule'
      },
      {
        path: 'addTimeLog/:id',
        loadChildren: './theme/time-log/time-log.module#TimeLogModule'
      },
      {
        path: '',
        redirectTo: 'employees',
        pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
